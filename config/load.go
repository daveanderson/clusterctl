package config

import (
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"net"

	yaml "gopkg.in/yaml.v2"
)

type yamlConfig struct {
	Name string
	IPv4 struct {
		Network        string
		Gateway        net.IP
		DNS            []net.IP
		OverlayNetwork string `yaml:"overlay-network"`
		ServiceNetwork string `yaml:"service-network"`
	}
	Machines []string
	Etcd     struct {
		Replicas []int
	}
	Kubernetes struct {
		Replicas []int
		Version  string
	}
}

func New(f string) (*Cluster, error) {
	bs, err := ioutil.ReadFile(f)
	if err != nil {
		return nil, err
	}
	return NewBytes(bs)
}

func NewBytes(b []byte) (*Cluster, error) {
	var ycfg yamlConfig
	err := yaml.Unmarshal(b, &ycfg)
	if err != nil {
		return nil, err
	}

	ret := &Cluster{
		Name: ycfg.Name,
		Etcd: &Etcd{},
		Kubernetes: &Kubernetes{
			Version: ycfg.Kubernetes.Version,
		},
	}
	ret.Etcd.cluster = ret
	ret.Kubernetes.cluster = ret

	ipv4, cidrv4, err := net.ParseCIDR(ycfg.IPv4.Network)
	if err != nil {
		return nil, fmt.Errorf("parsing IPv4 network address: %s", err)
	}
	ret.IPv4.Network = &net.IPNet{
		IP:   ipv4,
		Mask: cidrv4.Mask,
	}
	ret.IPv4.Gateway = ycfg.IPv4.Gateway
	ret.IPv4.DNS = ycfg.IPv4.DNS
	ipv4, cidrv4, err = net.ParseCIDR(ycfg.IPv4.OverlayNetwork)
	if err != nil {
		return nil, fmt.Errorf("parsing IPv4 overlay network address: %s", err)
	}
	ret.IPv4.OverlayNetwork = cidrv4

	ipv4, cidrv4, err = net.ParseCIDR(ycfg.IPv4.ServiceNetwork)
	if err != nil {
		return nil, fmt.Errorf("parsing IPv4 service network address: %s", err)
	}
	ret.IPv4.ServiceNetwork = cidrv4

	for i, macStr := range ycfg.Machines {
		mac, err := net.ParseMAC(macStr)
		if err != nil {
			return nil, err
		}
		ipAsInt := binary.BigEndian.Uint32(ret.IPv4.Network.IP.To4()) + uint32(i) + 1
		ip := &net.IPNet{
			IP:   make([]byte, 4),
			Mask: ret.IPv4.Network.Mask,
		}
		binary.BigEndian.PutUint32([]byte(ip.IP), ipAsInt)
		if !ret.IPv4.Network.Contains(ip.IP) {
			return nil, fmt.Errorf("Cannot fit %d machines in %s", i+1, ret.IPv4.Network)
		}

		ret.Machines = append(ret.Machines, &Machine{
			Hostname: fmt.Sprintf("m%d.%s", i+1, ret.Name),
			MAC:      mac,
			IPNet:    ip,
		})
	}

	for _, i := range ycfg.Etcd.Replicas {
		ret.Etcd.Replicas = append(ret.Etcd.Replicas, ret.Machines[i-1])
	}
	for _, i := range ycfg.Kubernetes.Replicas {
		ret.Kubernetes.Replicas = append(ret.Kubernetes.Replicas, ret.Machines[i-1])
	}

	return ret, nil
}
