package config

import (
	"fmt"
	"net"
	"strings"
)

type Cluster struct {
	Name string

	IPv4 struct {
		Network        *net.IPNet
		Gateway        net.IP
		DNS            []net.IP
		OverlayNetwork *net.IPNet
		ServiceNetwork *net.IPNet
	}

	Machines []*Machine

	Etcd       *Etcd
	Kubernetes *Kubernetes
}

type Machine struct {
	Hostname string
	MAC      net.HardwareAddr
	IPNet    *net.IPNet
}

type Etcd struct {
	Replicas []*Machine
	cluster  *Cluster
}

func (e *Etcd) URLs() string {
	ret := []string{}
	for i := range e.Replicas {
		ret = append(ret, fmt.Sprintf("https://%d.etcd.s.%s:2379", i, e.cluster.Name))
	}
	return strings.Join(ret, ",")
}

// OldConf generates a json etcd client configuration for the obsolete
// client that kubernetes still uses. It's either that, or use the new
// kubernetes that has no TLS support. Yes, seriously, because of
// course they just fucking delete features. Like a clown running
// across a minefield.
func (e *Etcd) OldConf() string {
	var peers []string
	for i := range e.Replicas {
		peers = append(peers, fmt.Sprintf(`"https://%d.etcd.s.%s:2379"`, i, e.cluster.Name))
	}
	return fmt.Sprintf(`
{ 
  "cluster": {
    "machines": [ %s ] 
  }, 
  "config": { 
    "certFile": "/etc/ssl/etcd/client.pem",
    "keyFile": "/etc/ssl/etcd/client-key.pem",
    "caCertFiles": ["/etc/ssl/etcd/ca.pem"],
    "consistency": "STRONG",
    "timeout": 1000000000
  } 
}
`, strings.Join(peers, ", "))
}

type Kubernetes struct {
	Replicas []*Machine
	Version  string
	cluster  *Cluster
}

func (k *Kubernetes) URLs() string {
	ret := []string{}
	for i := range k.Replicas {
		ret = append(ret, fmt.Sprintf("https://%d.kubernetes.s.%s:4430", i, k.cluster.Name))
	}
	return strings.Join(ret, ",")
}
