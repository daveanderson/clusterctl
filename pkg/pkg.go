package pkg

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"text/template"

	"go.universe.tf/clusterctl/config"
)

type Package struct {
	Files map[string]string
	Units map[string]bool
}

func FromTree(root string, cfg *config.Cluster, machine *config.Machine) (*Package, error) {
	ret := &Package{
		Files: map[string]string{},
		Units: map[string]bool{},
	}
	units := filepath.Join(root, "RUNUNITS")

	walk := func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		if path == units {
			bs, err := ioutil.ReadFile(path)
			if err != nil {
				return err
			}
			for _, unit := range strings.Split(string(bs), "\n") {
				unit = strings.TrimSpace(unit)
				if unit != "" {
					ret.Units[unit] = true
				}
			}
		}

		p := path[len(root)+1:]
		bs, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}
		tpl, err := template.New(p).Parse(string(bs))
		if err != nil {
			return err
		}
		var out bytes.Buffer
		err = tpl.Execute(&out, map[string]interface{}{
			"Cluster": cfg,
			"Machine": machine,
		})
		if err != nil {
			return err
		}
		ret.Files[p] = out.String()
		return nil
	}
	if err := filepath.Walk(root, walk); err != nil {
		return nil, err
	}

	return ret, nil
}

func (p *Package) DebugString() string {
	var out bytes.Buffer
	var paths []string
	for path := range p.Files {
		paths = append(paths, path)
	}
	sort.Strings(paths)
	for _, path := range paths {
		fmt.Fprintf(&out, "===== %s =====\n%s\n", path, p.Files[path])
	}
	fmt.Fprintln(&out, "===== ENABLED UNITS =====")
	for unit := range p.Units {
		fmt.Fprintln(&out, unit)
	}
	return out.String()
}
