export LOCKSMITHCTL_ENDPOINT={{.Cluster.Etcd.URLs}}
export LOCKSMITHCTL_ETCD_CAFILE=/etc/ssl/etcd/ca.pem
export LOCKSMITHCTL_ETCD_CERTFILE=/etc/ssl/etcd/client.pem
export LOCKSMITHCTL_ETCD_KEYFILE=/etc/ssl/etcd/client-key.pem
export ETCDCTL_ENDPOINT={{.Cluster.Etcd.URLs}}
export ETCDCTL_CA_FILE=/etc/ssl/etcd/ca.pem
export ETCDCTL_CERT_FILE=/etc/ssl/etcd/client.pem
export ETCDCTL_KEY_FILE=/etc/ssl/etcd/client-key.pem
