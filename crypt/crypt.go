// Package crypt is a wrapper around the crypt(3) libc function.
//
// This should really be in golang.org/x/crypto, the implementations
// can be tricky, and all the ones recommended by a google search have
// pretty terrible vulnerablities.
package crypt

// #cgo LDFLAGS: -lcrypt
// #define _GNU_SOURCE
// #include <crypt.h>
// #include <stdlib.h>
import "C"

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"unsafe"
)

// Crypt uses a custom base64 alphabet, because of course it does. The
// only difference from the standard alphabet is the last-but-one
// character is a . instead of a +.
var cryptAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789./"
var cryptEncoding = base64.NewEncoding(cryptAlphabet).WithPadding(base64.NoPadding)

// Crypt runs a password through crypt(3)'s SHA-512 KDF and returns
// the result.
func Crypt(password string) (string, error) {
	rawSalt := make([]byte, 12)
	if _, err := io.ReadFull(rand.Reader, rawSalt); err != nil {
		return "", err
	}
	salt := fmt.Sprintf("$6$%s", cryptEncoding.EncodeToString(rawSalt))
	return crypt(password, salt), nil
}

// crypt wraps C library crypt_r
func crypt(key, salt string) string {
	data := C.struct_crypt_data{}
	ckey := C.CString(key)
	csalt := C.CString(salt)
	out := C.GoString(C.crypt_r(ckey, csalt, &data))
	C.free(unsafe.Pointer(ckey))
	C.free(unsafe.Pointer(csalt))
	return out
}
