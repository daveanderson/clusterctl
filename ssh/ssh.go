package ssh

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"os"
	"path/filepath"

	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
)

type SSH struct {
	agentConn io.Closer
	client    *ssh.Client
}

func New(addr string) (*SSH, error) {
	agentConn, err := net.Dial("unix", os.Getenv("SSH_AUTH_SOCK"))
	if err != nil {
		return nil, err
	}
	agent := agent.NewClient(agentConn)

	cfg := &ssh.ClientConfig{
		User: "root",
		Auth: []ssh.AuthMethod{
			ssh.PublicKeysCallback(agent.Signers),
		},
	}

	client, err := ssh.Dial("tcp", addr+":22", cfg)
	if err != nil {
		agentConn.Close()
		return nil, err
	}

	return &SSH{agentConn, client}, nil
}

func (s *SSH) Close() error {
	s.agentConn.Close()
	return s.client.Close()
}

func (s *SSH) Run(cmd string) ([]byte, error) {
	sess, err := s.client.NewSession()
	if err != nil {
		return nil, err
	}
	defer sess.Close()
	return sess.Output(cmd)
}

func (s *SSH) RunAll(cmds []string) ([][]byte, error) {
	ret := make([][]byte, 0, len(cmds))
	for _, cmd := range cmds {
		out, err := s.Run(cmd)
		ret = append(ret, out)
		if err != nil {
			return ret, err
		}
	}
	return ret, nil
}

func (s *SSH) WriteFile(path string, mode os.FileMode, contents []byte) error {
	sess, err := s.client.NewSession()
	if err != nil {
		return err
	}
	defer sess.Close()
	w, err := sess.StdinPipe()
	if err != nil {
		return err
	}

	go func() {
		defer w.Close()
		fmt.Fprintf(w, "C%04o %d %s\n", mode&os.ModePerm, len(contents), filepath.Base(path))
		w.Write(contents)
		fmt.Fprintf(w, "\x00")
	}()

	cmd := fmt.Sprintf("/usr/bin/sudo /usr/bin/scp -t %s", path)
	out, err := sess.Output(cmd)
	if err != nil {
		return err
	}

	if !bytes.Equal(out, []byte{0, 0, 0}) {
		return fmt.Errorf("Remote SCP didn't ack as expected: wanted {0, 0, 0}, got %#v", out)
	}

	return nil
}
