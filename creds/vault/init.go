package vault

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"math/big"
	"time"

	"go.universe.tf/clusterctl/creds"
	vault "github.com/hashicorp/vault/api"
	"golang.org/x/crypto/ssh"
)

// Init generates CA keys for a new cluster. This function requires a
// root-level access token to run, because it mounts secret backends
// for the new CAs.
func (s *Store) Init() error {
	mounts, err := s.c.Sys().ListMounts()
	if err != nil {
		return err
	}

	root := fmt.Sprintf("cluster/%s/ca/ssh", s.cluster)
	if mounts[root] != nil {
		if mounts[root].Type != "generic" {
			return fmt.Errorf("A secret backend is already mounted at %q, but is not of type generic", root)
		}
	} else {
		err := s.c.Sys().Mount(root, &vault.MountInput{
			Type:        "generic",
			Description: fmt.Sprintf("SSH CA for %s", s.cluster),
		})
		if err != nil {
			return err
		}
	}
	sec, err := s.c.Logical().Read(root + "/key")
	if err != nil {
		return err
	}
	if sec != nil {
		return fmt.Errorf("Vault SSH CA already initialized")
	}
	sshCA, err := newSSHCA()
	if err != nil {
		return err
	}
	_, err = s.c.Logical().Write(root+"/key", map[string]interface{}{
		"pub": sshCA.Public,
		"key": sshCA.Private,
	})
	if err != nil {
		return err
	}

	root = fmt.Sprintf("cluster/%s/ca/etcd", s.cluster)
	if mounts[root] != nil {
		if mounts[root].Type != "pki" {
			return fmt.Errorf("A secret backend is already mounted at %q, but is not of type pki", root)
		}
	} else {
		err := s.c.Sys().Mount(root, &vault.MountInput{
			Type:        "pki",
			Description: fmt.Sprintf("Etcd CA for %s", s.cluster),
			Config: vault.MountConfigInput{
				MaxLeaseTTL: "876000h",
			},
		})
		if err != nil {
			return err
		}
	}
	etcdCA, err := newTLSCA(s.cluster)
	if err != nil {
		return err
	}
	_, err = s.c.Logical().Write(root+"/config/ca", map[string]interface{}{
		"pem_bundle": fmt.Sprintf("%s\n%s", etcdCA.Key, etcdCA.Certificate),
	})
	if err != nil {
		return err
	}

	role := map[string]interface{}{
		"allow_localhost":  false,
		"allow_any_name":   true,
		"allow_subdomains": true,
		"allow_ip_sans":    true,
		"server_flag":      false,
		"client_flag":      false,
	}
	role["server_flag"] = true
	role["client_flag"] = false
	if _, err = s.c.Logical().Write(root+"/roles/server", role); err != nil {
		return err
	}
	role["server_flag"] = false
	role["client_flag"] = true
	if _, err = s.c.Logical().Write(root+"/roles/client", role); err != nil {
		return err
	}
	role["server_flag"] = true
	role["client_flag"] = true
	if _, err = s.c.Logical().Write(root+"/roles/peer", role); err != nil {
		return err
	}

	root = fmt.Sprintf("cluster/%s/ca/kubernetes", s.cluster)
	if mounts[root] != nil {
		if mounts[root].Type != "pki" {
			return fmt.Errorf("A secret backend is already mounted at %q, but is not of type pki", root)
		}
	} else {
		err := s.c.Sys().Mount(root, &vault.MountInput{
			Type:        "pki",
			Description: fmt.Sprintf("Kubernetes CA for %s", s.cluster),
			Config: vault.MountConfigInput{
				MaxLeaseTTL: "876000h",
			},
		})
		if err != nil {
			return err
		}
	}
	k8sCA, err := newTLSCA(s.cluster)
	if err != nil {
		return err
	}
	_, err = s.c.Logical().Write(root+"/config/ca", map[string]interface{}{
		"pem_bundle": fmt.Sprintf("%s\n%s", k8sCA.Key, k8sCA.Certificate),
	})
	if err != nil {
		return err
	}

	role = map[string]interface{}{
		"allow_localhost":  false,
		"allow_any_name":   true,
		"allow_subdomains": true,
		"allow_ip_sans":    true,
		"server_flag":      false,
		"client_flag":      false,
	}
	role["server_flag"] = true
	role["client_flag"] = false
	if _, err = s.c.Logical().Write(root+"/roles/server", role); err != nil {
		return err
	}
	role["server_flag"] = false
	role["client_flag"] = true
	if _, err = s.c.Logical().Write(root+"/roles/client", role); err != nil {
		return err
	}
	role["server_flag"] = true
	role["client_flag"] = true
	if _, err = s.c.Logical().Write(root+"/roles/peer", role); err != nil {
		return err
	}

	return nil
}

// newSSHCA generates a new SSH CA keypair.
//
// It's actually just a regular keypair, since SSH doesn't have
// identifying marks for a CA pubkey vs. a regular one.
func newSSHCA() (*creds.SSHCred, error) {
	key, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return nil, err
	}
	pub, err := ssh.NewPublicKey(key.Public())
	if err != nil {
		return nil, err
	}
	keyPem := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(key),
	}
	return &creds.SSHCred{
		Private: string(pem.EncodeToMemory(keyPem)),
		Public:  string(ssh.MarshalAuthorizedKey(pub)),
	}, nil
}

// newTLSCA generates a new self-signed TLS CA cert.
//
// Vault 0.4 generates CA certs with an incorrect Extended Key Usages
// field, which makes client cert validation fail when using Go's TLS
// implementation or the Windows CryptoAPI. This is fixed in Vault
// 0.5, but 0.5 isn't released yet, so in the meantime, we generate
// the CA here by hand instead.
func newTLSCA(cluster string) (*creds.TLSCred, error) {
	key, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return nil, err
	}
	certTemplate := &x509.Certificate{
		SignatureAlgorithm: x509.SHA256WithRSA,
		Subject: pkix.Name{
			CommonName: cluster,
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().Add(3650 * 24 * time.Hour),
		KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageKeyEncipherment | x509.KeyUsageKeyAgreement | x509.KeyUsageCertSign | x509.KeyUsageCRLSign,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageAny},
		BasicConstraintsValid: true,
		IsCA: true,
	}
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		return nil, fmt.Errorf("failed to generate serial number: %s", err)
	}
	certTemplate.SerialNumber = serialNumber
	cert, err := x509.CreateCertificate(rand.Reader, certTemplate, certTemplate, &key.PublicKey, key)
	if err != nil {
		return nil, err
	}

	keyPem := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(key),
	}
	certPem := &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: cert,
	}

	return &creds.TLSCred{
		Key:         string(pem.EncodeToMemory(keyPem)),
		Certificate: string(pem.EncodeToMemory(certPem)),
	}, nil
}
