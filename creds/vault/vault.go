// Package vault implements creds.Store using Hashicorp Vault as a backend.
package vault

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"strings"
	"time"

	"go.universe.tf/clusterctl/creds"
	vault "github.com/hashicorp/vault/api"
	"golang.org/x/crypto/ssh"
)

type Store struct {
	c       *vault.Client
	cluster string
}

func NewFromEnv(cluster string) (*Store, error) {
	cfg := vault.DefaultConfig()
	if err := cfg.ReadEnvironment(); err != nil {
		return nil, err
	}
	v, err := vault.NewClient(cfg)
	if err != nil {
		return nil, err
	}
	return &Store{v, cluster}, nil
}

func (s *Store) SSH() creds.SSH {
	return &sshStore{s}
}

func (s *Store) Etcd() creds.TLS {
	return &tls{s, "etcd"}
}

func (s *Store) Kubernetes() creds.TLS {
	return &tls{s, "kubernetes"}
}

func (s *Store) Client() *vault.Client {
	return s.c
}

type sshStore struct {
	s *Store
}

func (s *sshStore) CA() (string, error) {
	path := fmt.Sprintf("cluster/%s/ca/ssh/key", s.s.cluster)
	sec, err := s.s.c.Logical().Read(path)
	if err != nil {
		return "", err
	}
	if sec == nil || sec.Data["pub"] == nil {
		return "", fmt.Errorf("SSH CA not found in Vault")
	}
	return sec.Data["pub"].(string), nil
}

func (s *sshStore) signer() (ssh.Signer, error) {
	path := fmt.Sprintf("cluster/%s/ca/ssh/key", s.s.cluster)
	sec, err := s.s.c.Logical().Read(path)
	if err != nil {
		return nil, err
	}
	if sec == nil || sec.Data["key"] == nil {
		return nil, errors.New("SSH CA not found in Vault")
	}
	b, _ := pem.Decode([]byte(sec.Data["key"].(string)))
	if b == nil || b.Type != "RSA PRIVATE KEY" {
		return nil, errors.New("Failed to decode SSH CA key")
	}
	key, err := x509.ParsePKCS1PrivateKey(b.Bytes)
	if err != nil {
		return nil, errors.New("Failed to decode SSH CA key")
	}

	return ssh.NewSignerFromKey(key)
}

func (s *sshStore) newKey(id string, ttl time.Duration) (*rsa.PrivateKey, *ssh.Certificate, error) {
	key, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, nil, err
	}
	pub, err := ssh.NewPublicKey(key.Public())
	if err != nil {
		return nil, nil, err
	}
	cert := &ssh.Certificate{
		Key:         pub,
		KeyId:       id,
		ValidAfter:  uint64(time.Now().Add(-5 * time.Minute).Unix()),
		ValidBefore: uint64(time.Now().Add(ttl).Unix()),
	}
	return key, cert, nil
}

func (s *sshStore) NewHost(id string, ttl time.Duration) (*creds.SSHCred, error) {
	ca, err := s.signer()
	if err != nil {
		return nil, err
	}
	key, cert, err := s.newKey(id, ttl)
	if err != nil {
		return nil, err
	}
	cert.CertType = ssh.HostCert
	if err = cert.SignCert(rand.Reader, ca); err != nil {
		return nil, err
	}

	keyPem := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(key),
	}
	return &creds.SSHCred{
		Private:     string(pem.EncodeToMemory(keyPem)),
		Public:      string(ssh.MarshalAuthorizedKey(cert.Key)),
		Certificate: string(ssh.MarshalAuthorizedKey(cert)),
	}, nil
}

func (s *sshStore) NewUser(id string, ttl time.Duration) (*creds.SSHCred, error) {
	ca, err := s.signer()
	if err != nil {
		return nil, err
	}
	key, cert, err := s.newKey(id, ttl)
	if err != nil {
		return nil, err
	}
	cert.CertType = ssh.UserCert
	cert.Extensions = map[string]string{
		"permit-X11-forwarding":   "",
		"permit-agent-forwarding": "",
		"permit-port-forwarding":  "",
		"permit-pty":              "",
		"permit-user-rc":          "",
	}
	if err = cert.SignCert(rand.Reader, ca); err != nil {
		return nil, err
	}

	keyPem := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(key),
	}
	return &creds.SSHCred{
		Private:     string(pem.EncodeToMemory(keyPem)),
		Public:      string(ssh.MarshalAuthorizedKey(cert.Key)),
		Certificate: string(ssh.MarshalAuthorizedKey(cert)),
	}, nil
}

type tls struct {
	s       *Store
	service string
}

func (t *tls) CA() (string, error) {
	resp, err := t.s.c.RawRequest(t.s.c.NewRequest("GET", fmt.Sprintf("/v1/cluster/%s/ca/%s/ca/pem", t.s.cluster, t.service)))
	if err != nil {
		return "", err
	}
	if resp.StatusCode != 200 {
		return "", fmt.Errorf("getting CA for %s returned HTTP status %d", t.service, resp.StatusCode)
	}
	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(bs), nil
}

func (t *tls) NewCert(hostnames []string, ip net.IP, use creds.TLSUse, ttl time.Duration) (*creds.TLSCred, error) {
	var role string
	switch use {
	case creds.TLSServer:
		role = "server"
	case creds.TLSClient:
		role = "client"
	case creds.TLSBoth:
		role = "peer"
	default:
		panic("Unknown PKI cert use")
	}

	if len(hostnames) < 1 {
		return nil, errors.New("Must have at least 1 hostname")
	}

	req := map[string]interface{}{
		"common_name": hostnames[0],
		"alt_names":   strings.Join(hostnames[1:], ","),
		"ttl":         fmt.Sprintf("%ds", int(ttl.Seconds())),
	}
	if ip != nil {
		req["ip_sans"] = ip.String()
	}

	resp, err := t.s.c.Logical().Write(fmt.Sprintf("cluster/%s/ca/%s/issue/%s", t.s.cluster, t.service, role), req)
	if err != nil {
		return nil, err
	}
	key := resp.Data["private_key"].(string)
	cert := resp.Data["certificate"].(string)
	return &creds.TLSCred{key, cert}, nil

}
