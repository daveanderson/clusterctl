package creds

import (
	"net"
	"time"
)

// Store issues credentials for base cluster services.
type Store interface {
	Init() error
	SSH() SSH
	Etcd() TLS
	Kubernetes() TLS
}

// SSH issues signed SSH keys from an SSH CA.
type SSH interface {
	// CA returns the CA's public key, in authorized_keys format.
	CA() (string, error)

	// NewHost issues a host SSH keypair signed by the CA.
	NewHost(id string, ttl time.Duration) (*SSHCred, error)

	// NewUser issues a user SSH keypair signed by the CA.
	NewUser(id string, ttl time.Duration) (*SSHCred, error)
}

// SSHCred is an SSH keypair signed by an SSH CA.
type SSHCred struct {
	Private     string // PEM encoded
	Public      string // authorized_keys format
	Certificate string // authorized_keys format
}

// TLSUse describes how a certificate may be used.
type TLSUse int

const (
	TLSClient TLSUse = iota
	TLSServer
	TLSBoth
)

// TLS issues certificates from a TLS CA.
type TLS interface {
	// CA returns the CA's certificate in PEM-encoded form.
	CA() (string, error)

	// NewCert issues a private key and certificate signed by the CA.
	NewCert(hostnames []string, ip net.IP, use TLSUse, ttl time.Duration) (*TLSCred, error)
}

// TLSCred is a TLS private key and certificate signed by a CA.
type TLSCred struct {
	Key         string // PEM encoded
	Certificate string // PEM encoded
}
