package install

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	ign "github.com/coreos/ignition/config"
	"go.universe.tf/clusterctl/config"
	"go.universe.tf/clusterctl/vault"
)

func Install(v *vault.Vault, c *config.Cluster, machnum int) error {
	m, err := c.Machine(machnum)
	if err != nil {
		return err
	}

	authorizedKey, err := v.SSHCA().AuthorizedKeys()
	if err != nil {
		return err
	}

	files, err := files(v, c, m)
	if err != nil {
		return err
	}

	sys, err := system(c, m)
	if err != nil {
		return err
	}

	cfg := ign.Config{
		Version: 1,
		Networkd: ign.Networkd{
			Units: network(c, m),
		},
		Systemd: ign.Systemd{
			Units: sys,
		},
		Storage: ign.Storage{
			Filesystems: []ign.Filesystem{
				{
					Device: "/dev/disk/by-partlabel/ROOT",
					Format: "ext4",
					Files:  files,
				},
			},
		},
		Passwd: ign.Passwd{
			Users: []ign.User{
				{
					Name:              "root",
					PasswordHash:      "$1$xyz$HmMN7hpwjfjSqin6OJUUo0",
					SSHAuthorizedKeys: []string{authorizedKey},
				},
				{
					Name:              "core",
					PasswordHash:      "$1$xyz$HmMN7hpwjfjSqin6OJUUo0",
					SSHAuthorizedKeys: []string{authorizedKey},
				},
			},
		},
	}

	bs, err := json.MarshalIndent(cfg, "", "  ")
	if err != nil {
		return err
	}
	v.Client().Logical().Write(fmt.Sprintf("cluster/%s/machstrap/%s", cfg.cluster.Name, m.MAC), map[string]interface{}{
		"install-device": "/dev/sda",
		"ignition":       string(bs),
	})
	fmt.Printf("%s (%s) scheduled for installation via Machstrap\n", m.Hostname, m.MAC)
	return nil
}

func network(c *config.Cluster, m *config.Machine) []ign.NetworkdUnit {
	var dns []string
	for _, d := range c.DNS {
		dns = append(dns, fmt.Sprintf("DNS=%s", d))
	}
	return []ign.NetworkdUnit{
		{
			Name: "00.network",
			Contents: fmt.Sprintf(`
[Match]
MACAddress=%s

[Network]
Address=%s
Gateway=%s
%s
`, m.MAC, m.IP.String(), c.Gateway, strings.Join(dns, "\n")),
		},
	}
}

func system(c *config.Cluster, m *config.Machine) ([]ign.SystemdUnit, error) {
	var etcds []string
	for i := range c.EtcdMachines {
		etcds = append(etcds, fmt.Sprintf("https://s%d.etcd.%s:2379", i+1, c.Name))
	}
	return []ign.SystemdUnit{
		{
			Name:   "locksmithd.service",
			Enable: true,
			DropIns: []ign.SystemdUnitDropIn{
				{
					Name: "etcd.conf",
					Contents: fmt.Sprintf(`
[Service]
Environment="LOCKSMITHD_ETCD_CAFILE=/etc/ssl/etcd/ca.pem"
Environment="LOCKSMITHD_ETCD_CERTFILE=/etc/ssl/etcd/client.pem"
Environment="LOCKSMITHD_ETCD_KEYFILE=/etc/ssl/etcd/client-key.pem"
Environment="LOCKSMITHD_ENDPOINT=%s"
`, strings.Join(etcds, ",")),
				},
			},
		},
		{
			Name:   "flanneld.service",
			Enable: true,
			DropIns: []ign.SystemdUnitDropIn{
				{
					Name: "etcd.conf",
					Contents: fmt.Sprintf(`
[Service]
Environment=FLANNEL_ENV_FILE=/etc/flannel.env
`),
				},
			},
		},
		{
			Name:   "docker.service",
			Enable: true,
			DropIns: []ign.SystemdUnitDropIn{
				{
					Name: "flannel.conf",
					Contents: `[Unit]
After=flanneld.service
Requires=flanneld.service
`,
				},
			},
		},
		{
			Name: "etcd2.service",
			DropIns: []ign.SystemdUnitDropIn{
				{
					Name: "etcd.conf",
					Contents: fmt.Sprintf(`
[Service]
Environment=ETCD_INITIAL_ADVERTISE_PEER_URLS=https://%s:2380
Environment=ETCD_ADVERTISE_CLIENT_URLS=https://%s:2379
Environment=ETCD_LISTEN_PEER_URLS=https://%s:2380
Environment=ETCD_LISTEN_CLIENT_URLS=https://%s:2379
Environment=ETCD_PEER_CERT_FILE=/etc/ssl/etcd/server.pem
Environment=ETCD_PEER_KEY_FILE=/etc/ssl/etcd/server-key.pem
Environment=ETCD_PEER_TRUSTED_CA_FILE=/etc/ssl/etcd/ca.pem
Environment=ETCD_PEER_CLIENT_CERT_AUTH=true
Environment=ETCD_CERT_FILE=/etc/ssl/etcd/server.pem
Environment=ETCD_KEY_FILE=/etc/ssl/etcd/server-key.pem
Environment=ETCD_TRUSTED_CA_FILE=/etc/ssl/etcd/ca.pem
Environment=ETCD_CLIENT_CERT_AUTH=true
		`, m.IP.IP, m.IP.IP, m.IP.IP, m.IP.IP),
				},
			},
		},
		{
			Name:   "get-hyperkube.service",
			Enable: true,
			Contents: `
[Unit]
Description=Fetch Kubernetes binary
After=early-docker.service network-online.target
Wants=network-online.target
Requires=early-docker.service

[Service]
Type=oneshot
RemainAfterExit=yes
Environment="DOCKER_HOST=unix:///var/run/early-docker.sock"
EnvironmentFile=/etc/k8s/environment
ExecStartPre=/usr/bin/mkdir -p /opt
ExecStart=/usr/bin/docker run --net=host --privileged=true --rm \
  --volume=/opt:/out \
  gcr.io/google_containers/hyperkube:${K8S_VERSION} cp -f /hyperkube /out

[Install]
WantedBy=multi-user.target
`,
		},
		{
			Name:   "kubelet.service",
			Enable: true,
			Contents: fmt.Sprintf(`
[Unit]
Description=Kubernetes Kubelet
After=flanneld.service docker.service get-hyperkube.service
Requires=flanneld.service docker.service get-hyperkube.service

[Service]
ExecStart=/opt/hyperkube kubelet \
  --api-servers=https://s1.k8s.%s \
  --kubeconfig=/etc/k8s/client.conf \
  --config=/etc/k8s/manifests \
  --logtostderr=true
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
`, c.Name),
		},
		{
			Name:   "kube-proxy.service",
			Enable: true,
			Contents: fmt.Sprintf(`
[Unit]
Description=Kubernetes node proxy
After=get-hyperkube.service
Requires=get-hyperkube.service

[Service]
ExecStart=/opt/hyperkube proxy \
  --kubeconfig=/etc/k8s/client.conf \
  --logtostderr=true
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
`),
		},
	}, nil
}

func files(v *vault.Vault, c *config.Cluster, m *config.Machine) (ret []ign.File, err error) {
	ssh, err := v.SSHCA().NewKey(m.Hostname, vault.SSHHostCert, 365*24*time.Hour)
	if err != nil {
		return nil, err
	}

	etcdkey, etcdcert, err := v.EtcdCA().Cert(fmt.Sprintf("s%d", m.Num), m.IP.IP, 365*24*time.Hour, vault.CertUseClient)
	if err != nil {
		return nil, err
	}

	etcdca, err := v.EtcdCA().CA()
	if err != nil {
		return nil, err
	}

	etcds := []string{}
	for i := range c.EtcdMachines {
		etcds = append(etcds, fmt.Sprintf("https://s%d.etcd.%s:2379", i+1, c.Name))
	}

	hosts, err := hostsFile(c)
	if err != nil {
		return nil, err
	}

	k8skey, k8scert, err := v.KubernetesCA().Cert(m.Hostname, m.IP.IP, 365*24*time.Hour, vault.CertUseClient)
	if err != nil {
		return nil, err
	}

	k8sca, err := v.KubernetesCA().CA()
	if err != nil {
		return nil, err
	}

	return []ign.File{
		// Base config
		{
			Path:     "/etc/hostname",
			Contents: m.Hostname,
			Mode:     0644,
		},
		{
			Path:     "/etc/hosts",
			Contents: hosts,
			Mode:     0644,
		},
		{
			Path:     "/etc/coreos/update.conf",
			Contents: "GROUP=alpha\nREBOOT_STRATEGY=etcd-lock",
			Mode:     0644,
		},
		// SSH
		{
			Path:     "/etc/ssh/ssh_host_rsa_key",
			Contents: ssh.PrivateString(),
			Mode:     0600,
		},
		{
			Path:     "/etc/ssh/ssh_host_rsa_key.pub",
			Contents: ssh.PublicString(),
			Mode:     0644,
		},
		{
			Path:     "/etc/ssh/ssh_host_rsa_key-cert.pub",
			Contents: ssh.CertificateString(),
			Mode:     0644,
		},
		{
			Path: "/etc/ssh/sshd_config",
			Contents: `
UsePrivilegeSeparation sandbox
Subsystem sftp internal-sftp
ClientAliveInterval 180
UseDNS no
HostCertificate /etc/ssh/ssh_host_rsa_key-cert.pub
PermitRootLogin yes
`,
			Mode: 0644,
		},
		// Etcd client credentials for root daemons on the machine
		{
			Path:     "/etc/ssl/etcd/client-key.pem",
			Contents: etcdkey,
			Mode:     0600,
		},
		{
			Path:     "/etc/ssl/etcd/client.pem",
			Contents: etcdcert,
			Mode:     0644,
		},
		{
			Path:     "/etc/ssl/etcd/ca.pem",
			Contents: etcdca,
			Mode:     0644,
		},
		{
			Path: "/root/.bash_profile",
			Contents: fmt.Sprintf(`
export LOCKSMITHCTL_ENDPOINT=%s
export LOCKSMITHCTL_ETCD_CAFILE=/etc/ssl/etcd/ca.pem
export LOCKSMITHCTL_ETCD_CERTFILE=/etc/ssl/etcd/client.pem
export LOCKSMITHCTL_ETCD_KEYFILE=/etc/ssl/etcd/client-key.pem
export ETCDCTL_CA_FILE=/etc/ssl/etcd/ca.pem
export ETCDCTL_CERT_FILE=/etc/ssl/etcd/client.pem
export ETCDCTL_KEY_FILE=/etc/ssl/etcd/client-key.pem
export ETCDCTL_ENDPOINT=%s
`, strings.Join(etcds, ","), strings.Join(etcds, ",")),
			Mode: 0644,
		},
		// Flannel
		{
			Path: "/etc/flannel.env",
			Contents: fmt.Sprintf(`
FLANNELD_ETCD_ENDPOINTS=%s
FLANNELD_ETCD_CAFILE=/etc/ssl/etcd/ca.pem
FLANNELD_ETCD_CERTFILE=/etc/ssl/etcd/client.pem
FLANNELD_ETCD_KEYFILE=/etc/ssl/etcd/client-key.pem
`, strings.Join(etcds, ",")),
			Mode: 0644,
		},
		// Kubernetes
		{
			Path:     "/etc/ssl/k8s/client-key.pem",
			Contents: k8skey,
			Mode:     0600,
		},
		{
			Path:     "/etc/ssl/k8s/client.pem",
			Contents: k8scert,
			Mode:     0644,
		},
		{
			Path:     "/etc/ssl/k8s/ca.pem",
			Contents: k8sca,
			Mode:     0644,
		},
		{
			Path: "/etc/k8s/environment",
			Contents: fmt.Sprintf(`
K8S_VERSION=%s
`, c.KubernetesVersion),
			Mode: 0644,
		},
		{
			Path: "/etc/k8s/client.conf",
			Contents: fmt.Sprintf(`
current-context: k8s
apiVersion: v1
clusters:
- cluster:
    certificate-authority: /etc/ssl/k8s/ca.pem
    server: https://s1.k8s.%s:443
  name: k8s
contexts:
- context:
    cluster: k8s
    namespace: k8s
    user: %s
  name: k8s
kind: Config
users:
- name: %s
  user:
    client-certificate: /etc/ssl/k8s/client.pem
    client-key: /etc/ssl/k8s/client-key.pem
`, c.Name, m.Hostname, m.Hostname),
			Mode: 0600,
		},
	}, nil
}

func hostsFile(c *config.Cluster) (string, error) {
	hostMap := map[string][]string{}

	addAliases := func(is []int, srv string) error {
		for i, midx := range is {
			m, err := c.Machine(midx)
			if err != nil {
				return err
			}

			ip := m.IP.IP.String()

			if _, ok := hostMap[ip]; !ok {
				hostMap[ip] = []string{m.Hostname}
			}
			hostMap[ip] = append(hostMap[ip], fmt.Sprintf("s%d.%s.%s", i+1, srv, c.Name))
		}
		return nil
	}

	if err := addAliases(c.EtcdMachines, "etcd"); err != nil {
		return "", err
	}
	if err := addAliases(c.KubernetesMasters, "k8s"); err != nil {
		return "", err
	}

	var ret []string
	for ip, aliases := range hostMap {
		ret = append(ret, fmt.Sprintf("%s %s", ip, strings.Join(aliases, " ")))
	}

	ret = append([]string{
		"127.0.0.1 localhost.localdomain localhost",
		"::1 localhost.localdomain localhost",
	}, ret...)

	return strings.Join(ret, "\n"), nil
}
