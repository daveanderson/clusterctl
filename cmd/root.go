package cmd

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"net"
	"net/http"
	"os"
	"time"

	etcd "github.com/coreos/etcd/client"
	"go.universe.tf/clusterctl/config"
	"go.universe.tf/clusterctl/creds"
	"go.universe.tf/clusterctl/creds/vault"
	vaultapi "github.com/hashicorp/vault/api"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"golang.org/x/net/context"
)

type cfgStruct struct {
	cluster *config.Cluster
	creds   creds.Store
	vault   *vaultapi.Client
}

func (c *cfgStruct) Etcd() (etcd.Client, error) {
	transport := &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		Dial: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).Dial,
		TLSHandshakeTimeout: 10 * time.Second,
		TLSClientConfig: &tls.Config{
			RootCAs:    x509.NewCertPool(),
			MinVersion: tls.VersionTLS10,
		},
	}

	ca, err := c.creds.Etcd().CA()
	if err != nil {
		return nil, err
	}
	if !transport.TLSClientConfig.RootCAs.AppendCertsFromPEM([]byte(ca)) {
		return nil, errors.New("failed to load Etcd CA")
	}

	etcdCert, err := c.creds.Etcd().NewCert([]string{"admin"}, nil, creds.TLSClient, 10*time.Minute)
	if err != nil {
		return nil, err
	}

	cert, err := tls.X509KeyPair([]byte(etcdCert.Certificate), []byte(etcdCert.Key))
	if err != nil {
		return nil, err
	}
	transport.TLSClientConfig.Certificates = []tls.Certificate{cert}

	cfg := etcd.Config{
		Transport: transport,
	}
	// We could use .Etcd.URLs(), but that's based on DNS names which
	// may not work from the machine clusterctl is on. So instead we
	// build a manual list with IP addresses.
	for _, m := range c.cluster.Etcd.Replicas {
		cfg.Endpoints = append(cfg.Endpoints, fmt.Sprintf("https://%s:2379", m.IPNet.IP))
	}

	etcdClient, err := etcd.New(cfg)
	if err != nil {
		return nil, err
	}

	// The on-demand certificate has a NotBefore time in the very
	// recent past. This can cause cert validation failures if there's
	// too much clock skew between the client and server. Ideally, NTP
	// would synchronize us to within an inch of our lives, but should
	// that fail, here's an icky workaround that retries for 5s in the
	// hopes that the cert becomes valid.
	for i := 0; i < 5; i++ {
		err = etcdClient.Sync(context.Background())
		if err == nil {
			return etcdClient, nil
		}
		time.Sleep(time.Second)
	}
	return nil, err
}

var cfg cfgStruct

func loadCfg(cmd *cobra.Command, args []string) error {
	cfgFile, err := cmd.Flags().GetString("config")
	if err != nil {
		return err
	}
	cfg.cluster, err = config.New(cfgFile)
	if err != nil {
		return err
	}

	v, err := vault.NewFromEnv(cfg.cluster.Name)
	if err != nil {
		return err
	}
	cfg.creds = v
	cfg.vault = v.Client()

	return nil
}

var RootCmd = &cobra.Command{
	Use:               "clusterctl",
	Short:             "Low-level management tool for Kubernetes clusters.",
	PersistentPreRunE: loadCfg,
	SilenceUsage:      true,
	SilenceErrors:     true,
}

func init() {
	viper.AutomaticEnv() // Read flags from the environment as well as the commandline
	RootCmd.PersistentFlags().StringP("config", "c", "./cluster.yaml", "config file (default is ./cluster.yaml)")
}

// Execute parses the commandline and executes the requested CLI
// command.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(-1)
	}
}
