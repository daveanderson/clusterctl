// Copyright © 2016 David Anderson <dave@natulte.net>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
	"time"

	etcd "github.com/coreos/etcd/client"
	"go.universe.tf/clusterctl/creds"
	"go.universe.tf/clusterctl/ssh"
	"github.com/spf13/cobra"
	"golang.org/x/net/context"
)

func etcdSetup(cmd *cobra.Command, args []string) error {
	newCluster, err := cmd.Flags().GetBool("create")
	if err != nil {
		return err
	}

	n, err := strconv.Atoi(args[0])
	if err != nil {
		return err
	}
	if n <= 0 || n > len(cfg.cluster.Etcd.Replicas) {
		return fmt.Errorf("invalid Etcd replica number '%d'", n)
	}

	mach := cfg.cluster.Etcd.Replicas[n-1]

	ssh, err := ssh.New(mach.IPNet.IP.String())
	if err != nil {
		return err
	}
	defer ssh.Close()

	cred, err := cfg.creds.Etcd().NewCert([]string{fmt.Sprintf("%d.etcd.s.%s", n-1, cfg.cluster.Name)}, mach.IPNet.IP, creds.TLSBoth, 365*24*time.Hour)
	if err != nil {
		return fmt.Errorf("creating Etcd client cert: %s", err)
	}

	if err = ssh.WriteFile("/etc/ssl/etcd/server.pem", 0644, []byte(cred.Certificate)); err != nil {
		return fmt.Errorf("writing server certificate: %s", err)
	}
	if err = ssh.WriteFile("/etc/ssl/etcd/server-key.pem", 0600, []byte(cred.Key)); err != nil {
		return fmt.Errorf("writing server key: %s", err)
	}

	if newCluster {
		err = ssh.WriteFile("/etc/systemd/system/etcd2.service.d/bootstrap.conf", 0644, []byte(fmt.Sprintf(`
[Service]
Environment=ETCD_NAME=etcd%d
Environment=ETCD_INITIAL_CLUSTER=etcd%d=https://%s:2380
Environment=ETCD_INITIAL_CLUSTER_STATE=new
`, n-1, n-1, mach.IPNet.IP)))
		if err != nil {
			return err
		}
	} else {
		e, err := cfg.Etcd()
		if err != nil {
			return fmt.Errorf("getting an etcd client: %s", err)
		}
		membersAPI := etcd.NewMembersAPI(e)
		// Get the current member list first, because adding a new
		// member might cause the cluster to lose quorum, so adding
		// must be the last thing we do the the cluster before
		// bringing up the new replica.
		members, err := membersAPI.List(context.Background())
		if err != nil {
			return err
		}
		_, err = membersAPI.Add(context.Background(), fmt.Sprintf("https://%s:2380", mach.IPNet.IP))
		if err != nil {
			return err
		}

		var peers []string
		for _, mem := range members {
			for _, url := range mem.PeerURLs {
				peers = append(peers, fmt.Sprintf("%s=%s", mem.Name, url))
			}
		}
		peers = append(peers, fmt.Sprintf("etcd%d=https://%s:2380", n-1, mach.IPNet.IP))

		err = ssh.WriteFile("/etc/systemd/system/etcd2.service.d/bootstrap.conf", 0644, []byte(fmt.Sprintf(`
[Service]
Environment=ETCD_NAME=etcd%d
Environment=ETCD_INITIAL_CLUSTER=%s
Environment=ETCD_INITIAL_CLUSTER_STATE=existing
`, n-1, strings.Join(peers, ","))))
		if err != nil {
			return err
		}
	}

	_, err = ssh.RunAll([]string{
		"systemctl stop etcd2",
		"chown etcd /etc/ssl/etcd/server-key.pem",
		"rm -rf /var/lib/etcd2/member",
		"systemctl daemon-reload",
		"systemctl start etcd2",
	})
	if err != nil {
		return err
	}

	return nil
}

func etcdTeardown(cmd *cobra.Command, args []string) error {
	machDown, err := cmd.Flags().GetBool("mach-down")
	if err != nil {
		return err
	}

	n, err := strconv.Atoi(args[0])
	if err != nil {
		return err
	}
	if n <= 0 || n > len(cfg.cluster.Etcd.Replicas) {
		return fmt.Errorf("invalid Etcd replica number '%d'", n)
	}

	mach := cfg.cluster.Etcd.Replicas[n-1]

	peerURL := fmt.Sprintf("https://%s:2380", mach.IPNet.IP)

	e, err := cfg.Etcd()
	if err != nil {
		return fmt.Errorf("connecting to Etcd: %s", err)
	}
	membersAPI := etcd.NewMembersAPI(e)

	// Get the current member list first, because adding a new
	// member might cause the cluster to lose quorum, so adding
	// must be the last thing we do the the cluster before
	// bringing up the new replica.
	members, err := membersAPI.List(context.Background())
	if err != nil {
		return err
	}

	var id string
findID:
	for _, member := range members {
		for _, purl := range member.PeerURLs {
			if purl == peerURL {
				id = member.ID
				break findID
			}
		}
	}
	if id == "" {
		return fmt.Errorf("%s doesn't seem to be a current member of the Etcd cluster", mach.Hostname)
	}

	var s *ssh.SSH
	if !machDown {
		s, err = ssh.New(mach.IPNet.IP.String())
		if err != nil {
			return err
		}
	}
	defer func() {
		if s != nil {
			s.Close()
		}
	}()

	// Now, start mutating. First, remove the replica from the
	// quorum. That replica will shut itself down once it discovers
	// the removal.
	err = membersAPI.Remove(context.Background(), id)
	if err != nil {
		return fmt.Errorf("removing %s from the Etcd cluster: %s", mach.Hostname, err)
	}

	// Then, if requested, clean up the machine.
	if !machDown {
		bs, err := s.RunAll([]string{
			"systemctl stop etcd2",
			"rm -f /etc/ssl/etcd/server-key.pem",
			"rm -f /etc/ssl/etcd/server.pem",
			"rm -f /etc/systemd/system/etcd2.service.d/bootstrap.conf",
			"rm -rf /var/lib/etcd2/member",
			"systemctl daemon-reload",
		})
		if err != nil {
			return fmt.Errorf("cleaning up on %s: %s\nCommand output: %s", mach.Hostname, err, string(bytes.Join(bs, []byte("\n\n"))))
		}
	}

	return nil
}

// etcdCmd represents the etcd command
var (
	etcdCmd = &cobra.Command{
		Use:   "etcd",
		Short: "Manage Etcd replicas",
	}
	etcdSetupCmd = &cobra.Command{
		Use:   "setup",
		Short: "Set up a new Etcd replica",
		RunE:  etcdSetup,
	}
	etcdTeardownCmd = &cobra.Command{
		Use:   "teardown",
		Short: "Tear down an Etcd replica",
		RunE:  etcdTeardown,
	}
)

func init() {
	RootCmd.AddCommand(etcdCmd)
	etcdCmd.AddCommand(etcdSetupCmd)
	etcdCmd.AddCommand(etcdTeardownCmd)

	etcdSetupCmd.Flags().Bool("create", false, "Create a new cluster instead of joining an existing one")
	etcdTeardownCmd.Flags().Bool("mach-down", false, "Assume the machine is unreachable, don't try to do on-machine cleanup")
}
