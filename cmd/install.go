package cmd

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"go.universe.tf/clusterctl/config"
	"go.universe.tf/clusterctl/creds"
	"go.universe.tf/clusterctl/pkg"
	"github.com/spf13/cobra"
)

func install(cmd *cobra.Command, args []string) error {
	var machs []*config.Machine
	for _, arg := range args {
		i, err := strconv.Atoi(arg)
		if err != nil {
			return fmt.Errorf("parsing machine index %q: %s", arg, err)
		}
		i--
		if i < 0 || i >= len(cfg.cluster.Machines) {
			return fmt.Errorf("machine index %d is out of bounds, must be 1-%d", i+1, len(cfg.cluster.Machines))
		}

		machs = append(machs, cfg.cluster.Machines[i])
	}

	failed := 0
	for _, mach := range machs {
		err := installMach(mach)
		if err != nil {
			failed++
			fmt.Printf("%s (%s) install scheduling failed: %s\n", mach.Hostname, mach.MAC, err)
		} else {
			fmt.Printf("%s (%s) install scheduled\n", mach.Hostname, mach.MAC)
		}
	}

	if failed > 0 {
		return fmt.Errorf("Failed to schedule installation of %d machines", failed)
	}
	return nil
}

func installMach(mach *config.Machine) error {
	p, err := pkg.FromTree("baseimg/flannel", cfg.cluster, mach)
	if err != nil {
		return fmt.Errorf("assembling base image files: %s", err)
	}

	type m map[string]interface{}
	var files []m
	for path, contents := range p.Files {
		files = append(files, m{
			"path":     path,
			"contents": contents,
			"mode":     0644,
		})
	}

	tlsca, err := cfg.creds.Etcd().CA()
	if err != nil {
		return fmt.Errorf("getting Etcd CA cert: %s", err)
	}
	tlscred, err := cfg.creds.Etcd().NewCert([]string{mach.Hostname}, mach.IPNet.IP, creds.TLSClient, 365*24*time.Hour)
	if err != nil {
		return fmt.Errorf("creating Etcd client cert: %s", err)
	}
	files = append(files, m{
		"path":     "/etc/ssl/etcd/ca.pem",
		"contents": tlsca,
		"mode":     0644,
	}, m{
		"path":     "/etc/ssl/etcd/client.pem",
		"contents": tlscred.Certificate,
		"mode":     0644,
	}, m{
		"path":     "/etc/ssl/etcd/client-key.pem",
		"contents": tlscred.Key,
		"mode":     0600,
	})

	tlsca, err = cfg.creds.Kubernetes().CA()
	if err != nil {
		return fmt.Errorf("getting Kubernetes CA cert: %s", err)
	}
	tlscred, err = cfg.creds.Kubernetes().NewCert([]string{mach.Hostname}, mach.IPNet.IP, creds.TLSClient, 365*24*time.Hour)
	if err != nil {
		return fmt.Errorf("creating Kubernetes client cert: %s", err)
	}
	files = append(files, m{
		"path":     "/etc/kubernetes/ssl/ca.pem",
		"contents": tlsca,
		"mode":     0644,
	}, m{
		"path":     "/etc/kubernetes/ssl/client.pem",
		"contents": tlscred.Certificate,
		"mode":     0644,
	}, m{
		"path":     "/etc/kubernetes/ssl/client-key.pem",
		"contents": tlscred.Key,
		"mode":     0600,
	})

	var units []m
	for unit := range p.Units {
		units = append(units, m{
			"name":   unit,
			"enable": true,
		})
	}

	sshcred, err := cfg.creds.SSH().NewHost(mach.Hostname, 365*24*time.Hour)
	if err != nil {
		return fmt.Errorf("creating SSH server cert: %s", err)
	}
	files = append(files, m{
		"path":     "/etc/ssh/ssh_cluster_rsa_key",
		"contents": sshcred.Private,
		"mode":     0600,
	}, m{
		"path":     "/etc/ssh/ssh_cluster_rsa_key.pub",
		"contents": sshcred.Public,
		"mode":     0644,
	}, m{
		"path":     "/etc/ssh/ssh_cluster_rsa_key-cert.pub",
		"contents": sshcred.Certificate,
		"mode":     0644,
	})

	sshCA, err := cfg.creds.SSH().CA()
	if err != nil {
		return fmt.Errorf("getting SSH CA public key: %s", err)
	}
	authorizedKey := fmt.Sprintf("cert-authority %s cluster-ssh-ca", strings.TrimSpace(sshCA))

	ignition := m{
		"ignitionVersion": 1,
		"storage": m{
			"filesystems": []m{
				{
					"device": "/dev/disk/by-partlabel/ROOT",
					"format": "ext4",
					"files":  files,
				},
			},
		},
		"systemd": m{
			"units": units,
		},
		"passwd": m{
			"users": []m{
				{
					"name":              "root",
					"sshAuthorizedKeys": []string{authorizedKey},
				},
				{
					"name":              "core",
					"sshAuthorizedKeys": []string{authorizedKey},
				},
			},
		},
	}

	s, err := cfg.vault.Logical().Read(fmt.Sprintf("cluster/%s/secrets/passwd", cfg.cluster.Name))
	if err == nil && s != nil && s.Data["root-password"] != "" {
		ignition["passwd"].(m)["users"].([]m)[0]["passwordHash"] = s.Data["root-password"]
	}

	bs, err := json.Marshal(ignition)
	if err != nil {
		return err
	}
	_, err = cfg.vault.Logical().Write(fmt.Sprintf("cluster/%s/machstrap/%s", cfg.cluster.Name, mach.MAC), m{
		"install-device": "/dev/sda",
		"ignition":       string(bs),
	})
	if err != nil {
		return err
	}

	return nil
}

var installCmd = &cobra.Command{
	Use:   "install",
	Short: "Schedule a machine for installation via Machstrap",
	RunE:  install,
}

func init() {
	RootCmd.AddCommand(installCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// installCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// installCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

}
