package cmd

import (
	"fmt"

	etcd "github.com/coreos/etcd/client"
	"go.universe.tf/clusterctl/crypt"
	vault "github.com/hashicorp/vault/api"
	"github.com/spf13/cobra"
	"golang.org/x/crypto/ssh/terminal"
	"golang.org/x/net/context"
)

func getPass(prompt string) (string, error) {
	fmt.Printf(prompt)
	pass, err := terminal.ReadPassword(0)
	if err != nil {
		return "", err
	}
	return string(pass), nil
}

func initCA(cmd *cobra.Command, args []string) error {
	fmt.Println(`Please enter a root password for the cluster. This password can only
be used for local logins, not SSH. Leave empty to disable password
logins completely.  `)

	rootPass, err := getPass("Cluster root password: ")
	if err != nil {
		return err
	}
	rootPass, err = crypt.Crypt(rootPass)
	if err != nil {
		return err
	}

	err = cfg.creds.Init()
	if err != nil {
		return err
	}

	err = cfg.vault.Sys().Mount(fmt.Sprintf("cluster/%s/machstrap", cfg.cluster.Name), &vault.MountInput{
		Type:        "generic",
		Description: fmt.Sprintf("Machstrap data for %s", cfg.cluster.Name),
	})
	if err != nil {
		return fmt.Errorf("mounting Machstrap data directory: %s", err)
	}

	err = cfg.vault.Sys().Mount(fmt.Sprintf("cluster/%s/secrets", cfg.cluster.Name), &vault.MountInput{
		Type:        "generic",
		Description: fmt.Sprintf("Secrets for %s", cfg.cluster.Name),
	})
	if err != nil {
		return fmt.Errorf("mounting secrets data directory: %s", err)
	}

	cfg.vault.Logical().Write(fmt.Sprintf("cluster/%s/secrets/passwd", cfg.cluster.Name), map[string]interface{}{
		"root-password": rootPass,
	})

	return nil
}

func initFlannel(cmd *cobra.Command, args []string) error {
	config := fmt.Sprintf(`{
  "Network": "%s",
  "Backend": {
    "Type": "host-gw"
  }
}`, cfg.cluster.IPv4.OverlayNetwork.String())

	e, err := cfg.Etcd()
	if err != nil {
		return err
	}

	_, err = etcd.NewKeysAPI(e).Create(context.Background(), "/coreos.com/network/config", config)
	if err != nil {
		return err
	}
	return nil
}

// initCmd represents the init command
var (
	initCmd = &cobra.Command{
		Use:   "init",
		Short: "One-time initialization steps",
	}
	caCmd = &cobra.Command{
		Use:   "ca",
		Short: "Create cluster CAs in Vault",
		Long:  `'clusterctl init ca' creates the certificate authorities required by the cluster.`,
		RunE:  initCA,
	}
	flannelCmd = &cobra.Command{
		Use:   "flannel",
		Short: "Write the cluster's Flannel configuration to etcd",
		Long: `'clusterctl init flannel' writes a flannel configuration to the cluster's Etcd.

This is an explicit operation because it needs to be run after the cluster etcd is operation.
`,
		RunE: initFlannel,
	}
)

func init() {
	RootCmd.AddCommand(initCmd)
	initCmd.AddCommand(caCmd)
	initCmd.AddCommand(flannelCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// initCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// initCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

}
