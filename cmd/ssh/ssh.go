package ssh

import (
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"strings"
	"time"

	"go.universe.tf/clusterctl/config"
	"go.universe.tf/clusterctl/vault"
	"golang.org/x/crypto/ssh/agent"
)

func Add(v *vault.Vault, c *config.Cluster) error {
	desc := fmt.Sprintf("%s cluster admin", c.Name)

	ssh, err := v.SSHCA().NewKey(desc, vault.SSHUserCert, 365*24*time.Hour)
	if err != nil {
		return err
	}

	agentConn, err := net.Dial("unix", os.Getenv("SSH_AUTH_SOCK"))
	if err != nil {
		return err
	}

	sshAgent := agent.NewClient(agentConn)
	err = sshAgent.Add(agent.AddedKey{
		PrivateKey:  ssh.Private,
		Certificate: ssh.Certificate,
		Comment:     desc,
	})
	if err != nil {
		return err
	}
	fmt.Printf("Loaded root credentials for %s into SSH agent\n", c.Name)
	return nil
}

func KnownHosts(v *vault.Vault, c *config.Cluster) error {
	kh, err := v.SSHCA().KnownHosts()
	if err != nil {
		return err
	}

	khFile := os.ExpandEnv("${HOME}/.ssh/known_hosts")

	bs, err := ioutil.ReadFile(khFile)
	if err != nil {
		return err
	}

	for _, l := range strings.Split(string(bs), "\n") {
		if l == kh {
			fmt.Println("known_hosts is already correct")
			return nil
		}
	}

	f, err := os.OpenFile(khFile, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0600)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.WriteString(kh + "\n")
	if err != nil {
		return err
	}

	fmt.Println("Added cluster CA to known_hosts")
	return nil
}
