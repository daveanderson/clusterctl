package k8s

import (
	"fmt"
	"strings"
	"time"

	"go.universe.tf/clusterctl/config"
	"go.universe.tf/clusterctl/ssh"
	"go.universe.tf/clusterctl/vault"
)

func setup(v *vault.Vault, c *config.Cluster, masternum int) error {
	if masternum <= 0 || masternum > len(c.KubernetesMasters) {
		return fmt.Errorf("Invalid Kubernetes master number '%d'", masternum)
	}

	m, err := c.Machine(c.KubernetesMasters[masternum-1])
	if err != nil {
		return err
	}

	ssh, err := ssh.NewSSH(m.IP.IP.String(), "root")
	if err != nil {
		return err
	}
	defer ssh.Close()

	key, cert, err := v.KubernetesCA().Cert("s1", m.IP.IP, 365*24*time.Hour, vault.CertUseServer)
	if err != nil {
		return err
	}
	if err = ssh.WriteFile("/etc/k8s/server.pem", 0644, []byte(cert)); err != nil {
		return err
	}
	if err = ssh.WriteFile("/etc/k8s/server-key.pem", 0600, []byte(key)); err != nil {
		return err
	}

	etcd := []string{}
	for i := range c.EtcdMachines {
		etcd = append(etcd, fmt.Sprintf("\"https://s%d.etcd.%s:2379\"", i+1, c.Name))
	}
	cfg := fmt.Sprintf(`{ 
  "cluster": {
    "machines": [ %s ] 
  }, 
  "config": { 
    "certFile": "/etc/ssl/etcd/client.pem", 
    "keyFile": "/etc/ssl/etcd/client-key.pem", 
    "caCertFiles": [ 
      "/etc/ssl/etcd/ca.pem" 
    ], 
    "timeout": 5000000000, 
    "consistency": "STRONG"
  } 
}`, strings.Join(etcd, ", "))
	if err = ssh.WriteFile("/etc/k8s/etcd.json", 0644, []byte(cfg)); err != nil {
		return err
	}

	cfg = fmt.Sprintf(`{
  "kind": "Pod",
  "apiVersion": "v1",
  "metadata": {
    "name": "kube-apiserver"
  },
  "spec": {
    "hostNetwork": true,
    "containers": [
      {
        "name": "kube-apiserver",
        "image": "gcr.io/google_containers/hyperkube:%s",
        "command": [
          "/hyperkube",
          "apiserver",
          "--admission-control=NamespaceLifecycle,LimitRanger,SecurityContextDeny,ServiceAccount,ResourceQuota",
          "--cluster-name=%s",
          "--etcd-config=/etc/k8s/etcd.json",
          "--tls-cert-file=/etc/k8s/server.pem",
          "--tls-private-key-file=/etc/k8s/server-key.pem",
          "--client-ca-file=/etc/k8s/ca.pem"
        ],
        "ports": [
          {
            "name": "https",
            "hostPort": 443,
            "containerPort": 443
          },
          {
            "name": "local",
            "hostPort": 8080,
            "containerPort": 8080
          }
        ],
        "volumeMounts": [
          {
            "name": "srvkube",
            "mountPath": "/srv/kubernetes",
            "readOnly": true
          },
          {
            "name": "etcssl",
            "mountPath": "/etc/ssl",
            "readOnly": true
          }
        ],
        "livenessProbe": {
          "httpGet": {
            "path": "/healthz",
            "port": 8080
          },
          "initialDelaySeconds": 15,
          "timeoutSeconds": 15
        }
      }
    ],
    "volumes": [
      {
        "name": "srvkube",
        "hostPath": {
          "path": "/srv/kubernetes"
        }
      },
      {
        "name": "etcssl",
        "hostPath": {
          "path": "/etc/ssl"
        }
      }
    ]
  }
}`, c.KubernetesVersion, c.Name)
	if err = ssh.WriteFile("/etc/k8s/manifests/apiserver.json", 0644, []byte(cfg)); err != nil {
		return err
	}

	cfg = fmt.Sprintf(`{
  "kind": "Pod",
  "apiVersion": "v1",
  "metadata": {
    "name": "kube-scheduler"
  },
  "spec": {
    "hostNetwork": true,
    "containers": [
      {
        "name": "kube-scheduler",
        "image": "gcr.io/google_containers/hyperkube:%s",
        "command": [
          "/hyperkube",
          "scheduler",
          "--master=127.0.0.1:8080"
        ],
        "livenessProbe": {
          "httpGet": {
            "host" : "127.0.0.1",
            "path": "/healthz",
            "port": 10251
          },
          "initialDelaySeconds": 15,
          "timeoutSeconds": 15
        }
      }
    ]
  }
}`, c.KubernetesVersion)
	if err = ssh.WriteFile("/etc/k8s/manifests/scheduler.json", 0644, []byte(cfg)); err != nil {
		return err
	}

	cfg = fmt.Sprintf(`{
  "kind": "Pod",
  "apiVersion": "v1",
  "metadata": {
    "name": "kube-controller-manager"
  },
  "spec": {
    "hostNetwork": true,
    "containers": [
      {
        "name": "kube-controller-manager",
        "image": "gcr.io/google_containers/hyperkube:%s",
        "command": [
          "/hyperkube",
          "controller-manager",
          "--cluster-name=%s",
          "--master=127.0.0.1:8080"
        ],
        "volumeMounts": [
          {
            "name": "srvkube",
            "mountPath": "/srv/kubernetes",
            "readOnly": true
          },
          {
            "name": "etcssl",
            "mountPath": "/etc/ssl",
            "readOnly": true
          }
        ],
        "livenessProbe": {
          "httpGet": {
            "host": "127.0.0.1",
            "path": "/healthz",
            "port": 10252
          },
          "initialDelaySeconds": 15,
          "timeoutSeconds": 15
        }
      }
    ],
    "volumes": [
      {
        "name": "srvkube",
        "hostPath": {
          "path": "/srv/kubernetes"
        }
      },
      {
        "name": "etcssl",
        "hostPath": {
          "path": "/etc/ssl"
        }
      }
    ]
  }
}`, c.KubernetesVersion)
	if err = ssh.WriteFile("/etc/k8s/manifests/scheduler.json", 0644, []byte(cfg)); err != nil {
		return err
	}
}
