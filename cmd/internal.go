// Copyright © 2016 David Anderson <dave@natulte.net>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"errors"
	"io/ioutil"

	vault "github.com/hashicorp/vault/api"
	"github.com/spf13/cobra"
)

func bootstrapVault(cmd *cobra.Command, args []string) error {
	initialized, err := cfg.vault.Sys().InitStatus()
	if err != nil {
		return err
	}

	if initialized {
		return errors.New("vault is already initialized")
	}

	resp, err := cfg.vault.Sys().Init(&vault.InitRequest{
		SecretShares:    1,
		SecretThreshold: 1,
	})
	if err != nil {
		return err
	}

	cfg.vault.SetToken(resp.RootToken)

	st, err := cfg.vault.Sys().Unseal(resp.Keys[0])
	if err != nil {
		return err
	}
	if st.Sealed {
		return errors.New("vault did not unseal as expected")
	}

	err = ioutil.WriteFile(args[0], []byte(resp.Keys[0]), 0600)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(args[1], []byte(resp.RootToken), 0600)
	if err != nil {
		return err
	}
	return nil
}

// internalCmd represents the internal command
var (
	internalCmd = &cobra.Command{
		Use:    "internal",
		Short:  "Internal commands for cluster-in-a-box use.",
		Hidden: true,
	}
	bootstrapVaultCmd = &cobra.Command{
		Use:   "bootstrap-vault",
		Short: "Initialize vault and write out the unseal key and root token",
		RunE:  bootstrapVault,
	}
)

func init() {
	RootCmd.AddCommand(internalCmd)
	internalCmd.AddCommand(bootstrapVaultCmd)
}
