package reboot

import (
	"fmt"

	"go.universe.tf/clusterctl/config"
	"go.universe.tf/clusterctl/ssh"
)

func Reboot(c *config.Cluster, machnum int, force bool) error {
	m, err := c.Machine(machnum)
	if err != nil {
		return err
	}

	s, err := ssh.NewSSH(m.IP.IP.String(), "root")
	if err != nil {
		return err
	}
	defer s.Close()

	var cmd, msg string
	if force {
		cmd = "reboot"
		msg = "Rebooted %s\n"
	} else {
		cmd = "locksmithctl send-need-reboot"
		msg = "Requested reboot of %s\n"
	}
	_, err = s.Run(cmd)
	if err != nil {
		return err
	}
	fmt.Printf(msg, m.Hostname)
	return nil
}
