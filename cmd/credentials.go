// Copyright © 2016 David Anderson <dave@natulte.net>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"os/user"
	"path/filepath"
	"strings"
	"time"

	"go.universe.tf/clusterctl/creds"
	"github.com/spf13/cobra"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
)

func sshCredToAgent(cred *creds.SSHCred) error {
	priv, err := ssh.ParseRawPrivateKey([]byte(cred.Private))
	if err != nil {
		return err
	}
	pub, _, _, _, err := ssh.ParseAuthorizedKey([]byte(cred.Certificate))
	if err != nil {
		return err
	}

	agentConn, err := net.Dial("unix", os.Getenv("SSH_AUTH_SOCK"))
	if err != nil {
		return err
	}

	sshAgent := agent.NewClient(agentConn)
	err = sshAgent.Add(agent.AddedKey{
		PrivateKey:  priv,
		Certificate: pub.(*ssh.Certificate),
		Comment:     fmt.Sprintf("cluster admin (%s)", cfg.cluster.Name),
	})
	if err != nil {
		return err
	}

	return nil
}

func sshCreds(cmd *cobra.Command, args []string) (err error) {
	var keyDesc string
	if cmd.Flags().Changed("keydesc") {
		keyDesc, err = cmd.Flags().GetString("keydesc")
		if err != nil {
			return err
		}
	} else {
		user, err := user.Current()
		if err != nil {
			return fmt.Errorf("getting current user: %s", err)
		}
		keyDesc = fmt.Sprintf("%s (%s)", user.Username, user.Name)
	}

	lifetime, err := cmd.Flags().GetDuration("lifetime")
	if err != nil {
		return err
	}

	cred, err := cfg.creds.SSH().NewUser(keyDesc, lifetime)
	if err != nil {
		return err
	}

	keyFile, err := cmd.Flags().GetString("keyfile")
	if err != nil {
		return err
	}
	if keyFile != "" {
		if err = ioutil.WriteFile(keyFile, []byte(cred.Private), 0600); err != nil {
			return fmt.Errorf("writing private key: %s", err)
		}
		if err = ioutil.WriteFile(keyFile+".pub", []byte(cred.Public), 0644); err != nil {
			return fmt.Errorf("writing public key: %s", err)
		}
		if err = ioutil.WriteFile(keyFile+"-cert.pub", []byte(cred.Certificate), 0644); err != nil {
			return fmt.Errorf("writing certificate: %s", err)
		}
	} else {
		if err = sshCredToAgent(cred); err != nil {
			return fmt.Errorf("writing key to SSH agent: %s", err)
		}
	}

	return nil
}

func etcdCreds(cmd *cobra.Command, args []string) error {
	certFile, err := cmd.Flags().GetString("certfile")
	if err != nil {
		return err
	}
	certFile, err = filepath.Abs(certFile)
	if err != nil {
		return err
	}
	keyFile, err := cmd.Flags().GetString("keyfile")
	if err != nil {
		return err
	}
	keyFile, err = filepath.Abs(keyFile)
	if err != nil {
		return err
	}
	caFile, err := cmd.Flags().GetString("cafile")
	if err != nil {
		return err
	}
	caFile, err = filepath.Abs(caFile)
	if err != nil {
		return err
	}
	lifetime, err := cmd.Flags().GetDuration("lifetime")
	if err != nil {
		return err
	}

	ca, err := cfg.creds.Etcd().CA()
	if err != nil {
		return fmt.Errorf("getting Etcd CA certificate: %s", err)
	}
	cred, err := cfg.creds.Etcd().NewCert([]string{"admin"}, nil, creds.TLSClient, lifetime)
	if err != nil {
		return fmt.Errorf("issuing Etcd client credentials: %s", err)
	}

	if err = ioutil.WriteFile(caFile, []byte(ca), 0644); err != nil {
		return fmt.Errorf("writing Etcd CA to %s: %s", caFile, err)
	}
	if err = ioutil.WriteFile(certFile, []byte(cred.Certificate), 0644); err != nil {
		return fmt.Errorf("writing client certificate to %s: %s", certFile, err)
	}
	if err = ioutil.WriteFile(keyFile, []byte(cred.Key), 0600); err != nil {
		return fmt.Errorf("writing client key to %s: %s", keyFile, err)
	}

	e, err := cfg.Etcd()
	// Set the authentication envvars even if getting the Etcd
	// endpoints failed.
	fmt.Printf("export ETCDCTL_CA_FILE=%s\n", caFile)
	fmt.Printf("export ETCDCTL_CERT_FILE=%s\n", certFile)
	fmt.Printf("export ETCDCTL_KEY_FILE=%s\n", keyFile)
	if err != nil {
		return fmt.Errorf("Failed to set ETCDCTL_ENDPOINT: %s", err)
	}
	fmt.Printf("export ETCDCTL_ENDPOINT=%s\n", strings.Join(e.Endpoints(), ","))

	return nil
}

// credentialsCmd represents the credentials command
var (
	credentialsCmd = &cobra.Command{
		Use:     "credentials",
		Aliases: []string{"creds"},
		Short:   "Manage client credentials for cluster services",
	}
	credsSSHCmd = &cobra.Command{
		Use:     "ssh",
		Aliases: []string{"ssh-add"},
		Short:   "Issue root SSH credentials",
		RunE:    sshCreds,
	}
	credsEtcdCmd = &cobra.Command{
		Use:   "etcd",
		Short: "Issue Etcd client credentials",
		RunE:  etcdCreds,
	}
)

func init() {
	RootCmd.AddCommand(credentialsCmd)
	credentialsCmd.AddCommand(credsSSHCmd)
	credentialsCmd.AddCommand(credsEtcdCmd)

	credsSSHCmd.Flags().String("keyfile", "", "Write the SSH key to a file (the public key and certificate are written to KEYFILE.pub and KEYFILE-cert.pub)")
	credsSSHCmd.Flags().String("keydesc", "", "Key description to write in the certificate (logged by the remote system)")
	credsSSHCmd.Flags().Duration("lifetime", 20*time.Hour, "Certificate validity period")

	credsEtcdCmd.Flags().String("certfile", "etcd-client.pem", "Write the client certificate to this file")
	credsEtcdCmd.Flags().String("keyfile", "etcd-client-key.pem", "Write the client key to this file")
	credsEtcdCmd.Flags().String("cafile", "etcd-client-ca.pem", "Write the Etcd CA certificate to this file")
	credsEtcdCmd.Flags().Duration("lifetime", 20*time.Hour, "Certificate validity period")
}
