package status

import (
	"fmt"
	"net"
	"strings"
	"time"

	"go.universe.tf/clusterctl/config"
	"go.universe.tf/clusterctl/ssh"
	"go.universe.tf/clusterctl/vault"
)

func Status(v *vault.Vault, c *config.Cluster, machIndices []int) error {
	machs := make([]*config.Machine, len(machIndices))
	for i, midx := range machIndices {
		m, err := c.Machine(midx)
		if err != nil {
			return err
		}
		machs[i] = m
	}

	type res struct {
		i   int
		ok  bool
		msg string
	}
	results := make([]string, len(machs))
	ch := make(chan res, len(machIndices))

	for i, m := range machs {
		go func(i int, m *config.Machine) {
			res := res{i, false, ""}
			defer func() {
				ch <- res
			}()

			ign, err := v.Client().Logical().Read(fmt.Sprintf("machstrap/%s", m.MAC))
			if err != nil {
				res.msg = err.Error()
				return
			}
			if ign != nil && ign.Data != nil && ign.Data["ignition"] != nil {
				res.msg = "awaiting installation"
				return
			}

			dialer := &net.Dialer{
				Timeout: 3 * time.Second,
			}
			c, err := dialer.Dial("tcp", fmt.Sprintf("%s:22", m.IP.IP))
			if err != nil {
				res.msg = err.Error()
				return
			}
			c.Close()

			s, err := ssh.NewSSH(m.IP.IP.String(), "root")
			if err != nil {
				res.msg = err.Error()
				return
			}
			defer s.Close()
			bs, err := s.Run("source /etc/lsb-release && echo $DISTRIB_DESCRIPTION")
			if err != nil {
				res.msg = err.Error()
				return
			}
			res.ok = true
			res.msg = fmt.Sprintf("OK (%s)", strings.TrimSpace(string(bs)))
		}(i, m)
	}

	ok := true
	for _ = range machs {
		res := <-ch
		ok = ok && res.ok
		results[res.i] = fmt.Sprintf("%s: %s", machs[res.i].Hostname, res.msg)
	}

	fmt.Println(strings.Join(results, "\n"))
	if !ok {
		return fmt.Errorf("Some machines not healthy")
	}
	return nil
}
