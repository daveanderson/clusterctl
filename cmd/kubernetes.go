package cmd

import (
	"fmt"
	"strconv"
	"time"

	"go.universe.tf/clusterctl/creds"
	"go.universe.tf/clusterctl/ssh"
	"github.com/spf13/cobra"
)

func kubernetesSetup(cmd *cobra.Command, args []string) error {
	n, err := strconv.Atoi(args[0])
	if err != nil {
		return err
	}
	if n <= 0 || n > len(cfg.cluster.Kubernetes.Replicas) {
		return fmt.Errorf("invalid Etcd replica number '%d'", n)
	}

	mach := cfg.cluster.Kubernetes.Replicas[n-1]

	ssh, err := ssh.New(mach.IPNet.IP.String())
	if err != nil {
		return err
	}
	defer ssh.Close()

	cred, err := cfg.creds.Kubernetes().NewCert([]string{fmt.Sprintf("%d.kubernetes.s.%s", n-1, cfg.cluster.Name)}, mach.IPNet.IP, creds.TLSServer, 365*24*time.Hour)
	if err != nil {
		return fmt.Errorf("creating Kubernetes server cert: %s", err)
	}

	if err = ssh.WriteFile("/etc/kubernetes/ssl/apiserver.pem", 0644, []byte(cred.Certificate)); err != nil {
		return fmt.Errorf("writing server certificate: %s", err)
	}
	if err = ssh.WriteFile("/etc/kubernetes/ssl/apiserver-key.pem", 0600, []byte(cred.Key)); err != nil {
		return fmt.Errorf("writing server key: %s", err)
	}

	_, err = ssh.RunAll([]string{
		"for i in /etc/kubernetes/master-manifests/*; do ln -sf $i /etc/kubernetes/manifests; done",
	})
	if err != nil {
		return err
	}

	return nil
}

var (
	kubernetesCmd = &cobra.Command{
		Use:   "kubernetes",
		Short: "Manage Kubernetes masters",
	}
	kubernetesSetupCmd = &cobra.Command{
		Use:   "setup",
		Short: "Set up a new Kubernetes replica",
		RunE:  kubernetesSetup,
	}
)

func init() {
	RootCmd.AddCommand(kubernetesCmd)
	kubernetesCmd.AddCommand(kubernetesSetupCmd)
}
